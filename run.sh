docker build -t elastic_project .

# create the network
docker network create elasticnetwork

docker run -d --net elasticnetwork -p 9200:9200 -p 9300:9300 --name es elasticsearch:6.5.1

# start the django app container
docker run -d --net elasticnetwork -p 8000:8000 --name elastic_django_project elastic_project
